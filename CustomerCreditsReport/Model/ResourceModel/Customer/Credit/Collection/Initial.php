<?php

namespace Surge\CustomerCreditsReport\Model\ResourceModel\Customer\Credit\Collection;

class Initial extends \Surge\CustomerCreditsReport\Model\ResourceModel\Report\Collection
{
    /**
     * Report sub-collection class name
     *
     * @var string
     */
    protected $_reportCollection = 'Surge\CustomerCreditsReport\Model\ResourceModel\Customer\Credit\Collection';
}